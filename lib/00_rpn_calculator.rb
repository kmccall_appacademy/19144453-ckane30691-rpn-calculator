class RPNCalculator
  OPERANDS = [:+, :-, :*, :/]

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    execute_op(:+)
  end

  def minus
    execute_op(:-)
  end

  def divide
    execute_op(:/)
  end

  def times
    execute_op(:*)
  end

  def tokens(string)
    string.split(" ").map do |char| 
      operand?(char) ? char.to_sym : char.to_i
    end
  end

  def value
    @stack.last
  end

  def evaluate(string)
    tokens(string).each do |token|
      token.is_a?(Integer) ? push(token): execute_op(token)
    end
    value
  end

  private

  def execute_op(symbol)
    raise "calculator is empty" if @stack.length < 2
    right_operand = @stack.pop
    left_operand = @stack.pop
    case symbol
    when :+
      @stack << left_operand + right_operand
    when :-
      @stack << left_operand - right_operand
    when :*
      @stack << left_operand * right_operand
    when :/
      @stack << left_operand.fdiv(right_operand)
    else
      @stack << left_op << right_op
      raise "bad operator"
    end
    self
  end

  def operand?(character)
    OPERANDS.include?(character.to_sym)
  end

end
